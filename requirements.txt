Django==2.0.4
pytz==2018.3
mysqlclient==1.3.12
six==1.11.0
django-bootstrap3==9.1.0
pillow==5.1.0
