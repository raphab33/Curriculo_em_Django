from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm
from .models import Profile

class ProfileForm(UserCreationForm):
    class Meta:
    	model = Profile
    	fields = ['username','nome', 'vinculo', 'exper', 'curreal', 'email', 
    	'telefone', 'curso', 'periodo', 'funcao','habilidades','disponibilidade','photo']


class ProfileForm2(ModelForm):
    class Meta:
    	model = Profile
    	fields = ['nome', 'vinculo', 'exper', 'curreal', 'email', 
    	'telefone', 'curso', 'periodo', 'funcao','habilidades','disponibilidade','photo']    	