from django.urls import path
from .views import index, cadastrar, update_profile, visualizar_perfil, pin


urlpatterns = [
	
	path('', index, name="index"),
    path('cadastrar/', cadastrar, name='cadastrar'),
    path('update_profile/<int:id>/', update_profile, name='update_profile'),
    path('visualizar_perfil/<str:username>/', visualizar_perfil, name='visualizar_perfil'),
    path('pin', pin, name="pin"),

]