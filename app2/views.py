from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from .models import Profile, Pin
from .forms import ProfileForm, ProfileForm2
import pickle
from django.template.loader import render_to_string




def pin(request, template_name="pin.html"):

       
    pin_lista = Pin.objects.all()
    pin = {"pin": pin_lista[0]}
    
    return render(request,template_name , pin)

def index(request, template_name='index.html'):
    
    profile = Profile.objects.all()
    count = Profile.objects.all().count()
    user = request.user 
    dados = {'lista_profiles': profile, 'count': count, 'user': user}

    return render(request, template_name, dados)

    


def cadastrar(request):

    if request.method == 'POST':

        form = ProfileForm(request.POST or None, request.FILES or None)

        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = ProfileForm()

    return render(request, 'cadastro.html', {'formulario': form})    


@login_required
def update_profile(request, id, template_name='cadastro.html'):
    profile = get_object_or_404(Profile, pk=id)
    form = ProfileForm2(request.POST or None, request.FILES or None, instance=profile)
    user = request.user.id
    if  user == id:
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
       messages.error(request, 'Permissão negada.')
       return redirect('index')
    return render(request, template_name, {'formulario': form})

@login_required
def visualizar_perfil(request, username, template_name='perfil_usuario.html'):
    dados = Profile.objects.filter(username=username)
    user = request.user

    usuario = {'lista': dados}
    return render(request, template_name, usuario, user)

def logout_view(request):
    logout(request)
    return redirect('index')
    
    















# def logar(request, template_name="login.html"):
#     next = request.GET.get('next', '/listar_usuario/')
#     if request.method == "POST":
#         username = request.POST['username']
#         password = request.POST['password']
#         user = authenticate(username=username, password=password)
#         if user is not None:
#             login(request, user)
#             return HttpResponseRedirect(next)

#         else:
#             messages.error(request, 'Usuário ou senha incorretos.')
#             return HttpResponseRedirect(settings.LOGIN_URL)

#     return render(request, template_name, {'redirect_to': next})


# @login_required
# def registrar_usuario(request, template_name="registrar.html"):
#     user = request.user
#     if user.is_staff:
#         if request.method == "POST":
#             username = request.POST['username']
#             email = request.POST['email']
#             password = request.POST['password']
#             tipo = request.POST['tipo_usuario']
#             if tipo == "administrador":
#                 user = User.objects.create_user(username, email, password)
#                 user.is_staff = True
#                 user.save()
#             else:
#                 User.objects.create_user(username, email, password)

#             return redirect('/listar_usuario/')
#     else:
#         messages.error(request, 'Permissão negada.')
#         return redirect('/listar_usuario/')

#     return render(request, template_name, {})


# #@login_required # Habilita função de login
# def index(request, template_name='index.html'):
    
#     profile = Profile.objects.all()
#     dados = {'lista_profiles': profile}
        
#     return render(request, template_name, dados)


# @login_required
# def listar_usuario(request, template_name="listar.html"):
#     usuarios = User.objects.all()
#     usuario = {'lista': usuarios}
#     return render(request, template_name, usuario)




# @login_required
# def remover_usuario(request, pk, template_name='delete.html'):
#     user = request.user
#     if user.has_perm('user.delete_user'):
#         try:
#             usuario = User.objects.get(pk = pk)
#             if request.method == "POST":
#                 usuario.delete()
#                 return redirect('listar_usuario')
#         except:
#             messages.error(request, 'Usuário não encontrado.')
#             return redirect('/listar_usuario/')
#     else:
#         messages.error(request, 'Permissão negada.')
#         return redirect('/listar_usuario/')

#     return render(request, template_name, {'usuario': usuario})

# def deslogar(request):
#     logout(request)
#     return HttpResponseRedirect(settings.LOGIN_URL)

# @login_required
# def perfil_usuario(request):
#     user = request.user
#     template_name = "editar_usuario.html"
#     if request.method == "POST":
#         nome = request.POST['nome']
#         email = request.POST['email']
#         password = request.POST['password']
#         habilidades = request.POST['habilidades']
#         telefone = request.POST['telefone']
#         curso = request.POST['curso']
#         periodo = request.POST['periodo']
#         funcao = request.POST['funcao']
#         exper = request.POST['exper']
#         curreal = request.POST['curreal']
#         tipo = request.POST['disponibilidade_usuario']
#         if tipo == "disponivel":
#             user.profile.disponibilidade = True
#         else:
#             user.profile.disponibilidade = False
#         user.set_password(password)
#         user.profile.nome = nome
#         user.profile.habilidades = habilidades
#         user.profile.email = email
#         user.profile.telefone = telefone
#         user.profile.curso = curso
#         user.profile.periodo = periodo
#         user.profile.funcao = funcao
#         user.profile.exper = exper
#         user.profile.curreal = curreal
#         user.save()
#         return redirect('/listar_usuario/')

#     return render(request, template_name, {})

# @login_required
# def conta_usuario(request, pk, template_name = "conta_usuario.html"):
#     dados = Profile.objects.filter(pk = pk)
#     usuario = {'lista': dados}
#     return render(request, template_name, usuario)

   



#     ######views do outro sistema



# @login_required
# def new_clients(request, template_name='client_form.html'):
#     form = ClienteForm(request.POST or None, request.FILES or None)

#     if form.is_valid():
#         form.save()
#         return redirect('list_clients')
#     return render(request, template_name, {'form': form})



# @login_required
# def update_clients(request, id, template_name='client_form.html'):
#     cliente = get_object_or_404(Cliente, pk=id)
#     form = ClienteForm(request.POST or None, request.FILES or None, instance=cliente)

#     if form.is_valid():
#         form.save()
#         return redirect('list_clients')
#     return render(request, template_name, {'form': form})

# @login_required
# def delete_clients(request, id, template_name='cliente_delete.html'):
#     cliente = get_object_or_404(Cliente, pk=id)

#     if request.method == 'POST':
#         cliente.delete()
#         return redirect('list_clients')
#     return render(request, template_name, {'cliente': cliente})
