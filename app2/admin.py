from django.contrib import admin
from .models import Profile, Pin

admin.site.register(Profile)
admin.site.register(Pin)