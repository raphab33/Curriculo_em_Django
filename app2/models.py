# -*- coding utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class Pin(models.Model):
    pin = models.CharField(verbose_name='PIN', max_length=30, blank=True, null=True)
    
    def __str__(self):
        return self.pin
        
class Profile(User):
    
    nome = models.CharField(verbose_name='Nome Completo',max_length=50, blank=True, null=True)
    vinculo = models.CharField(verbose_name='Vínculo', max_length=50)
    exper = models.TextField(verbose_name='Experiência',max_length=1000, blank=True, null=True)
    curreal = models.TextField(verbose_name='Cursos Realizados',max_length=1000, blank=True, null=True)
    telefone = models.CharField(verbose_name='Celular',max_length=12, blank=True, null=True)
    curso = models.CharField(verbose_name='Graduação',max_length=20, blank=True, null=True)
    periodo = models.CharField(verbose_name='Periodo',max_length=2, blank=True, null=True)
    funcao = models.CharField(verbose_name='Função',max_length=20, blank=True, null=True)
    habilidades = models.CharField(verbose_name='Habilidades',max_length=100, blank=True, null=True)
    disponibilidade = models.BooleanField(verbose_name='Disponibilidade', default=True)
    photo = models.ImageField(upload_to='profile_photos', null=True, blank=True)
    

    def __unicode__(self):
        return self.nome

